Установка 

1. Клонируйте репозиторий пакета
2. установить «pip install pip» (если не установлен)
3. установить пакет «python -m pip install -r requirements.txt»
4. создать и запустить миграции: 
    1. «python manage.py makemigrations»  
    2.  «python manage.py migrate»


Установка и запуск клиент-демона

1. Скопировать фалы в папку /opt/test_cupp/ файлы start.sh и proc.sh
2. В файле start.sh задайте адрес и порт порта сервера $server 
3. В файле start.sh задайте $TestPath путь, по которому сервер обрабатывает urls.py python-пакета test. Например ‘test_utilize/‘
3. Скопируйте в папку /etc/systemd/system/ файл testutilize.service
4. Запустите клиент-демон командой systemctl start test
	
