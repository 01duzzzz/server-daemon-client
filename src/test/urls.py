from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from .views import TestList, TestDetail, TestCreate, CountView

urlpatterns =[
    path('list/', TestList.as_view()),
    path('<int:pk>', TestDetail.as_view()),
    path('add/', TestCreate.as_view()),
    path('', CountView.as_view())
]