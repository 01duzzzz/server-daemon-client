from rest_framework import generics
from .modelse import TestData
from .serializers import TestSerializer
from django.views import View
from django.shortcuts import render


class TestDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TestSerializer
    queryset = TestData.objects.all()


class TestCreate(generics.CreateAPIView):
    serializer_class = TestSerializer
    queryset = TestData.objects.all()


class TestList(generics.ListAPIView):
    serializer_class = TestSerializer
    queryset = TestData.objects.get_100()


class CountView(View):
    def get(self, request):
        data = TestData.objects.get_100()
        count_all = TestData.objects.get_stat_all()
        count_100 = TestData.objects.get_stat_100()
        return render(
            request,
            'test/index.html',
            context={'data': data, 'count_all': count_all, 'count_100': count_100}
        )