from rest_framework import serializers

from .modelse import TestData


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestData
        fields = "__all__"